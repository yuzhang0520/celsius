package sheridan;

public class Celsius {
	
	public static int convertFromFahrenheit(int fahrenheit) {
		float calculate = ((float)5/9) * (fahrenheit - 32);
		int celsius = Math.round(calculate);
		return celsius;
	}
	

	public static void main(String[] args) {
		System.out.println("Celsius converted from Fahrenheit is: " + Celsius.convertFromFahrenheit(100));
	}

}
