package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertFromFahrenheit() {
		int celsiusShouldBe = -12;
		assertEquals(celsiusShouldBe, Celsius.convertFromFahrenheit(10));
	}
	
	@Test
	public void testConvertFromFahrenheitExceptional() {
		int celsiusShouldBe = -13;
		assertNotEquals(celsiusShouldBe, Celsius.convertFromFahrenheit(10));
	}
	
	@Test
	public void testConvertFromFahrenheitBoundaryIn() {
		int celsiusShouldBe = -14;
		assertEquals(celsiusShouldBe, Celsius.convertFromFahrenheit(7));
	}
	
	@Test
	public void testConvertFromFahrenheitBoundaryOut() {
		int celsiusShouldBe = -14;
		assertEquals(celsiusShouldBe, Celsius.convertFromFahrenheit(6));
	}

}
